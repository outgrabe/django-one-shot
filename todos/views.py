from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/todos.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list_object = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_object)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list_object.id)
    else:
        form = TodoListForm(instance=todo_list_object)
    context = {
        "todo_list_object": todo_list_object,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_object = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_object.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    todo_list_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_list_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list_item.list.id)
    else:
        form = TodoItemForm(instance=todo_list_item)
    context = {
        "todo_list_item": todo_list_item,
        "form": form,
    }
    return render(request, "todos/items/edit.html", context)
